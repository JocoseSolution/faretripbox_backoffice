﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="OldLedgerReport.aspx.vb" Inherits="SprReports_Admin_OldLedgerReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }
    </style>
    <script type="text/javascript">
        function CheckFormVal() {
            var agencyfield = $("#txtAgencyName").val();
            if (agencyfield == "") {
                alert("Please Enter Agency Name or ID !");
                return false;
            }
        }
    </script>

    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Old Ledger Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" id="tr_Agency" runat="server">
                                    <label for="exampleInputEmail1">Agency</label>
                                    <span id="tr_AgencyName" runat="server">
                                        <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                            onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <label for="exampleInputEmail1">Trans Type :</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="Charge">Charge</asp:ListItem>
                                    <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4 pull-right">
                                            <br/>
                                <div class="w40 lft">
                                    <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button buttonBlue" OnClientClick="return CheckFormVal();" />

                                </div>
                                <div class="w20 lft">&nbsp;</div>
                                <div class="w40 lft">
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" />
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table" PageSize="30">
                                            <Columns>
                                                <asp:BoundField DataField="Reseller_Name" HeaderText="Agency_Name" />
                                                <asp:BoundField DataField="Cart_ID" HeaderText="Cart ID" />
                                                <asp:BoundField DataField="Payment_Time" HeaderText="Payment Time" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                                <asp:BoundField HeaderText="Reason" DataField="Reason"></asp:BoundField>
                                                <asp:BoundField HeaderText="Operation" DataField="Operation" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Pay_Amount" DataField="Pay_Amount" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Payment_Fee" DataField="Payment_Fee" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Service_Tax/GST" DataField="Service_Tax_GST" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="TDS_Amount" DataField="TDS_Amount" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Net_Amount" DataField="Net_Amount"></asp:BoundField>
                                                <asp:BoundField HeaderText="Deposit" DataField="Deposit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Deposit_Limit" DataField="Deposit_Limit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="Passenger_Names" DataField="Passenger_Names" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>

